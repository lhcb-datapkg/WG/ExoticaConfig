2022-01-26 - WG/ExoticaConfig v1r20
========================================

## Updates and new features:

 - Add B-mesogenesis and DM filtering script (a2d8de8c2173a219488954730d65b569b674e9a3 @acasaisv).

