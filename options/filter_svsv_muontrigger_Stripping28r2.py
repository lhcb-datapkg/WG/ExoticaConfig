"""
Stripping28r2 filtered production for DiJetSVSV events 

Outputs to DST.

Retention rate of 9% when tested on dijet=b,m70GeV,mu_tight (49000090) events.

@author Will Barter
@date   2022-04-17
"""

#stripping version
stripping='stripping28r2'

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

from StrippingSelections.Utils import lineBuilder, buildStreams
streams = buildStreams( config, WGs = ['QEE'])

AllStreams = StrippingStream("AllSVSVL0MuonEW.Strip")

linesToAdd = []
for stream in streams:
    for line in stream.lines:
        if ( 'HltQEEJetsDiJetSVSV' in line.name() ):
        		line._prescale = 1.0
        		linesToAdd.append(line) 
AllStreams.appendLines(linesToAdd)
sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip' )

for i in linesToAdd:
    print(i.name())
    

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 #selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)
# https://gitlab.cern.ch/lhcb-datapkg/AppConfig/-/blob/master/options/DaVinci/DV-Stripping28r2-Stripping-MC-DST.py#L87

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
#
# DaVinci Configuration
#
from Configurables import DaVinci

from Configurables import LoKi__L0Filter as L0Filter
fltrs = L0Filter('MuonEW', Code = "L0_CHANNEL('MuonEW')" )
DaVinci().EventPreFilters = [ fltrs]

DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

'''
## tested with `lb-run DaVinci/v44r10p5 gaudirun.py filter_svsv_muontrigger_Stripping28r2.py`
DaVinci().DataType = "2016"
from GaudiConf import IOHelper
IOHelper().inputFiles([
    '/eos/lhcb/user/w/wbarter/testarea/00147478_00000612_7.AllStreams.dst',
    '/eos/lhcb/user/w/wbarter/testarea/00147478_00000100_7.AllStreams.dst'
    ], clear=True)
'''
