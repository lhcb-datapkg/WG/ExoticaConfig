from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/EW', '/Event/Strip' ]

stripping='stripping34r0p2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)

dm_lines = []
for stream in streams:
  for line in stream.lines:
    if 'DMLambda' in line.name(): 
      dm_lines.append(line)


dm = StrippingStream("BhadronCompleteEvent")
dm.appendLines(dm_lines)

sc = StrippingConf(Streams=[dm],
                   MaxCandidates=-1,
                   AcceptBadEvents = False,
                   TESPrefix = 'Strip'
                   )

enablePacking = True
dstExtension = ".dst"

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default' : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default' : stripDSTStreamConf(pack=enablePacking,
                                   selectiveRawEvent=True,
                                   fileExtension = '.dst')
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = "00000",
			              SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44113402)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
## DaVinci Configuration
#
from Configurables import DaVinci

DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().DataType = "2018"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([ stck ])
DaVinci().appendToMainSequence([ dstWriter.sequence() ])


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

