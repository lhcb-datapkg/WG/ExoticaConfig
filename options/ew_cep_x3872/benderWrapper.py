""" 
This script is run inside the Bender environment. It expects the prodConf_DaVinci_ file to exist,
and uses that to find the data and xml catalogue. It expects the year of data-taking as an argument
"""

import re
import glob
import subprocess
import sys

# Get year as argument
if len(sys.argv) != 2 or sys.argv[1] not in ["2011","2012","2015","2016","2017","2018"] :
  print "Need year argument"
  sys.exit()

# Load the production configuration so we know what to do
prod_conf_matches = glob.glob('prodConf_DaVinci_*.py')
assert len(prod_conf_matches) == 1, prod_conf_matches
with open(prod_conf_matches[0], 'rt') as fp:
    prod_conf = fp.read()

# Find the XMLFileCatalog filename
matches = re.findall("XMLFileCatalog=['\"]([^'\"]+\.xml)['\"]", prod_conf)
assert len(matches) == 1, (matches, prod_conf)
xml_catalog_fn = matches[0]

# Find the OutputFileTypes
matches = re.findall("OutputFilePrefix=['\"]([^'\"]+)['\"]", prod_conf)
assert len(matches) == 1, (matches, prod_conf)
output_prefix = matches[0]

matches = re.findall("OutputFileTypes=\[['\"]([^'\"]+)['\"]\]", prod_conf)
assert len(matches) == 1, (matches, prod_conf)
output_filetype = matches[0]

output_filename = output_prefix + '.' + output_filetype

# Find the LFNs
lfns = re.findall("['\"](LFN:/[^'\"]+)['\"]", prod_conf)
assert lfns, (lfns, prod_conf)

# Now get the module, configure, and run
from Bender.Awesome import *
from XJpsi import configure

configure ( lfns , catalogs = [xml_catalog_fn] , castor = False , params = { 'OutputFileName' : output_filename, 'Simulation': False , 'State': 'X_1(3872)','FinalState':'DiMuonDiPion' , 'Year' : sys.argv[1] } )

run(-1)

