# Dummy DaVinci options
from Configurables import DaVinci
DaVinci().SkipEvents = 10000000000000
DaVinci().DataType = '2016'
DaVinci().Lumi = False
DaVinci().Simulation = False
DaVinci().EvtMax = 0

# Now back to business
import subprocess

return_code = subprocess.call( "source /cvmfs/lhcb.cern.ch/lib/LbEnv && lb-run -c best Bender/v33r1p2 python $EXOTICACONFIGROOT/options/ew_cep_x3872/benderWrapper.py 2016" 
                             , shell=True )

if return_code not in [0, 2]:
  raise RuntimeError("Got bad return code from Bender "+str(return_code))
