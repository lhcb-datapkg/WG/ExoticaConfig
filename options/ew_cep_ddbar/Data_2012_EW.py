"""
Helper file to define data type for davinci
"""
# Need this to allow import from the module in the WG package
import os
import sys
importPath = os.getcwd() + "/DaVinciDev_v42r4/WG/EWConfig/python"
sys.path.insert(0,importPath)

the_year      = '2012'
the_fileType  = 'DST'

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
             )


##############################
# Event filtering for data
##############################

# Add code to filter the input if not simulation
from PhysConf.Filters import LoKi_Filters
from Configurables import LoKi__L0Filter as L0Filter
from Configurables import LoKi__HDRFilter   as HltFilter

# L0 filtering
#fltrs_l0  = L0Filter(  'L0PassFilter',  Code = " L0_CHANNEL  ( 'DiHadron,lowMult' )  " )

# HLT filtering
hltFilterString = "HLT_PASS_RE('Hlt2LowMultD.*Decision')"
fltrs_hlt = HltFilter( 'HltPassFilter', Code = hltFilterString , Location="Hlt2/DecReports")

# GaudiSequencer to contain the L0 and HLT filters
from Configurables import GaudiSequencer
filteredSeq = GaudiSequencer('filteredSeq');
#filteredSeq.Members = [fltrs_l0, fltrs_hlt]
filteredSeq.Members = [fltrs_hlt]

##############################
# Do final configuration
##############################

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from ew_cep_ddbar.makeCEP_DD import configure_ew_cep_dd_selection

# Get the dictionary of algorithms
algorithmDict = configure_ew_cep_dd_selection( the_year )

# Now prepare an OR sequencer for the selection and tupling sequences
seq_candidateSelectionAndTupling = GaudiSequencer("seq_candidateSelectionAndTupling")
seq_candidateSelectionAndTupling.IgnoreFilterPassed = True
seq_candidateSelectionAndTupling.Members += algorithmDict["selTuples"]
filteredSeq.Members += [ seq_candidateSelectionAndTupling ]

# Put the list of event tuple (unfiltered) and scale&smear/selection/tupling (filtered) algorithms in the UserAlgorithms list
dv.UserAlgorithms += algorithmDict["eventTuple"] + [ filteredSeq ]

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year
