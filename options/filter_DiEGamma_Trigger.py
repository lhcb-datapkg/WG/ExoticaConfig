"""
Option file to filter simulated data based on L0, HLT1, and HLT2 trigger decisions.
Note that the HLT2 lines were only added in 2018.
@author Michael K. Wilkinson <michael.k.wilkinson@cern.ch>
@date 2024-12-05
"""


# trigger filter
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_CHANNEL_RE('Electron.*')",
    HLT1_Code = "(HLT_PASS_RE('Hlt1.*Electron.*Decision') | HLT_PASS_RE('Hlt1.*TrackMVA.*Decision'))",    
    HLT2_Code = "(HLT_PASS_RE('Hlt2ExoticaPi0ToDiEGammaDecision') | HLT_PASS_RE('Hlt2ExoticaEtaToDiEGammaDecision'))"
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci

fltrSeq = trigfltrs.sequence ( 'TrigFilters' )
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# configure the copy-stream algorithm
from GaudiConf import IOHelper
copy = IOHelper().outputAlgs("hltfilter.dst" , 'InputCopyStream/INPUTCOPY' )
copy[0].AcceptAlgs = DaVinci().EventPreFilters
from Configurables import ApplicationMgr
app = ApplicationMgr ( OutStream = copy )
