###################################################################
# Trigger filtering on Topo
# Author: Marcin Kucharczyk, 16-04-2012
###################################################################
#
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    HLT_Code = "HLT_PASS_RE('Hlt2.*Topo.*Decision')" 
    )

from Configurables import DaVinci
DaVinci().EventPreFilters = trigfltrs.filters('topoFilters')
