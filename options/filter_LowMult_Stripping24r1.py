#### Re-run Stripping24r1 on EW stream and save only LowMult lines ####

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/EW', '/Event/Strip' ]

stripping='stripping24r1'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)

lowMultLines = []
for stream in streams:
  for line in stream.lines:
    if 'LowMult' in line.name(): lowMultLines.append(line)

lowmult     = StrippingStream("LowMult.EW")
lowmult.appendLines(lowMultLines)


from Configurables import ProcStatusCheck, GaudiSequencer

stripTESPrefix = 'Strip'
sc = StrippingConf( Streams = [ lowmult ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    TESPrefix = stripTESPrefix )

enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements )

SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf     = {'default' : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True, fileExtension='.dst') }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams() )

from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x38162410)

from Configurables import DaVinci
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ eventNodeKiller ] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

from Configurables import AuditorSvc, TimingAuditor, SequencerTimerTool, NameAuditor
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

DaVinci().EvtMax     = -1
DaVinci().DataType   = '2015'
DaVinci().InputType  = 'DST'
DaVinci().Simulation = False

