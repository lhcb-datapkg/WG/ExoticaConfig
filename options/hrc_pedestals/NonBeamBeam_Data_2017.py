# Helper file to define data type for davinci

# Ganga hack to allow importing of the module
import os
importPath = os.getcwd() + "/DaVinciDev_v44r1/WG/ExoticaConfig/python"
import sys
sys.path.insert(0,importPath)

the_year      = '2017'
the_fileType  = 'DST'
the_sample    = "NonBeamBeam"

# ================================
# Configure DaVinci
# ================================
from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
             )

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from hrc_pedestals.pedestals_and_dimu import configure_hrc_pedestals_and_dimu_selection
dv.appendToMainSequence( configure_hrc_pedestals_and_dimu_selection( dataYear = the_year , sample = the_sample ) ) 

db = CondDB  ( LatestGlobalTagByDataType = the_year )


# ================================
# Set appropriate filtering
# ================================
from hrc_pedestals.eventFilters import getFilters
if len(getFilters( the_year , the_sample )) != 0 :
  dv.EventPreFilters = getFilters( the_year , the_sample )

if '__main__' == __name__ :

  print ' The year : ', the_year

