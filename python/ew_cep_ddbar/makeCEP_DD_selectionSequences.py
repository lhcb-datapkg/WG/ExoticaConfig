def makeSelectionSequences() :
    from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
    from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles

    #=============================================#
    #=== General containers ======================#
    #=============================================#
    # Kaons
    dod_stdKaons  = DataOnDemand(Location = "Phys/StdAllNoPIDsKaons/Particles")
    fltr_stdKaons = FilterDesktop(Code = "(PT > 100.0) & (P > 5000.0) & (TRCHI2DOF < 4.0) & (PIDK > 0.0) & (TRGHOSTPROB<0.4)")
    myStdKaons    = Selection("myStdKaons", Algorithm = fltr_stdKaons, RequiredSelections = [dod_stdKaons])
    # Pions
    dod_stdPions  = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
    fltr_stdPions = FilterDesktop(Code = "(PT > 100.0) & (P > 5000.0) & (TRCHI2DOF < 4.0) & (TRGHOSTPROB<0.4)")
    myStdPions    = Selection("myStdPions", Algorithm = fltr_stdPions, RequiredSelections = [dod_stdPions])
    # Gamma
    dod_stdGamma            = DataOnDemand(Location = "Phys/StdLooseAllPhotons/Particles")
    fltr_stdGamma           = FilterDesktop(Code = "((PT > 150*MeV) & (CL > 0.4) & (PPINFO(LHCb.ProtoParticle.IsNotE,-1) > -999.0))")
    myStdGamma              = Selection(name="myStdGamma", Algorithm = fltr_stdGamma , RequiredSelections=[dod_stdGamma])
    # Resolved Pi0's
    dod_stdPi0R   = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
    fltr_stdPi0R  = FilterDesktop(Code = "(PT > 100.0) & (CHILD(CL, 1) > 0.2) & (CHILD(CL, 2) > 0.2)");
    myStdPi0R     = Selection(name="myStdPi0R", Algorithm = fltr_stdPi0R, RequiredSelections=[dod_stdPi0R])
    # Merged Pi0's
    dod_stdPi0M   = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")
    fltr_stdPi0M  = FilterDesktop(Code = "(PT > 200.0)");
    myStdPi0M     = Selection(name="myStdPi0M", Algorithm = fltr_stdPi0M, RequiredSelections=[dod_stdPi0M])

    sels={}
    #=============================================#
    #=== Make D0 -> Kpi candidates (and WS) ======#
    #=============================================#
    CombineD2KPi   = CombineParticles(
        DecayDescriptors = [ "[D0 -> K- pi+ ]cc" ],
        CombinationCut   = "(APT > 0.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
        MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    CombineD2KPiWS = CombineParticles(
        DecayDescriptors = [ "[D0 -> K+ pi+ ]cc" ],
        CombinationCut   = "(APT > 0.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
        MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    sels["sel_D0_KPi"]       = Selection(name = "sel_D0_KPi"   , Algorithm=CombineD2KPi  , RequiredSelections = [myStdKaons,myStdPions])
    sels["sel_D0_KPi_WS"]    = Selection(name = "sel_D0_KPi_WS", Algorithm=CombineD2KPiWS, RequiredSelections = [myStdKaons,myStdPions])

    #=============================================#
    #=== Make D0 -> K3pi candidates (and WS) =====#
    #=============================================#
    CombineD2K3Pi   = CombineParticles(
    DecayDescriptors = [ "[D0 -> K- pi+ pi+ pi- ]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    CombineD2K3PiWS = CombineParticles(
    DecayDescriptors = [ "[D0 -> K+ pi+ pi+ pi+]cc","[D0 -> K+ pi+ pi+ pi-]cc","[D0 -> K+ pi- pi- pi-]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    sels["sel_D0_K3Pi"]       = Selection(name = "sel_D0_K3Pi"   , Algorithm=CombineD2K3Pi  , RequiredSelections = [myStdKaons,myStdPions])
    sels["sel_D0_K3Pi_WS"]    = Selection(name = "sel_D0_K3Pi_WS", Algorithm=CombineD2K3PiWS, RequiredSelections = [myStdKaons,myStdPions])

    #=============================================#
    #=== Make D+ -> Kpipi candidates (and WS) ====#
    #=============================================#
    CombineD2KPiPi  = CombineParticles(
    DecayDescriptors = [ "[D+ -> K- pi+ pi+]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    CombineD2KPiPiWS= CombineParticles(
    DecayDescriptors = [ "[D+ -> K+ pi+ pi+]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    sels["sel_Dp_KPiPi"]       = Selection(name = "sel_D0_KPiPi"   , Algorithm=CombineD2KPiPi  , RequiredSelections = [myStdKaons,myStdPions])
    sels["sel_Dp_KPiPi_WS"]    = Selection(name = "sel_D0_KPiPi_WS", Algorithm=CombineD2KPiPiWS, RequiredSelections = [myStdKaons,myStdPions])

    #=============================================#
    #=== Make Ds+ -> KKpi candidates (and WS) ====#
    #=============================================#
    CombineDs2KKPi  = CombineParticles(
    DecayDescriptors = [ "[D_s+ -> K- K+ pi+]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    CombineDs2KKPiWS= CombineParticles(
    DecayDescriptors = [ "[D_s+ -> K+ K+ pi+]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")

    sels["sel_Dsp_KKPi"]       = Selection(name = "sel_Dsp_KKPi"   , Algorithm=CombineDs2KKPi  , RequiredSelections = [myStdKaons,myStdPions])
    sels["sel_Dsp_KKPi_WS"]    = Selection(name = "sel_Dsp_KKPi_WS", Algorithm=CombineDs2KKPiWS, RequiredSelections = [myStdKaons,myStdPions])

    #=====================================#
    #=== Make D0->Kpipi0 candidates ======#
    #=====================================#
    CombineD2KPiPi0 = CombineParticles(#ParticleCombiners = {'':'LoKi::VertexFitter'},
    DecayDescriptors = [ "[D0 -> K- pi+ pi0]cc" ],
    CombinationCut   = "(APT > 100.0) & (ADAMASS('D0') < 80.0) & (AP > 10000.0) & (ADOCAMAX('LoKi::DistanceCalculator') < 0.5)",
    MotherCut        = "(VFASPF(VCHI2PDOF) < 15.0)")


    sels["sel_D0_KPiPi0R"] = Selection(name = "sel_D0_KPiPi0R", Algorithm=CombineD2KPiPi0, RequiredSelections = [myStdKaons,myStdPions,myStdPi0R])
    sels["sel_D0_KPiPi0M"] = Selection(name = "sel_D0_KPiPi0M", Algorithm=CombineD2KPiPi0, RequiredSelections = [myStdKaons,myStdPions,myStdPi0M])

    #=============================================#
    #====== Make D*+(->Kpi,K3pi)pi+ candidates ===#
    #=============================================#
    CombineDstarp2D0pip = CombineParticles(DecayDescriptors = ["[D*(2010)+ -> D0 pi+]cc"],
    CombinationCut = "(ADAMASS('D*(2010)+') < 100.0)",
    MotherCut = "((M - M1) > 135) & ((M - M1) < 200) & (VFASPF(VCHI2PDOF) < 15)")
    sels["sel_Dstar_KPi"]      = Selection(name = "sel_Dstar_KPi",   Algorithm = CombineDstarp2D0pip, RequiredSelections= [myStdPions,sels["sel_D0_KPi"]])
    sels["sel_Dstar_K3Pi"]     = Selection(name = "sel_Dstar_K3Pi",  Algorithm = CombineDstarp2D0pip, RequiredSelections= [myStdPions,sels["sel_D0_K3Pi"]])

    #=============================================#
    #====== Make D*0 -> D0(->Kpi) pi0 candidates =#
    #=============================================#
    CombineDstar02D0pi0 = CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 pi0]cc"],
    CombinationCut = "(ADAMASS('D*(2007)0') < 100.0)",
    MotherCut = "((M - M1) > 135) & ((M - M1) < 200) & (VFASPF(VCHI2PDOF) < 15)")
    sels["sel_Dstar0_Pi0R_KPi"]      = Selection(name = "sel_Dstar0_Pi0R_KPi",   Algorithm = CombineDstar02D0pi0, RequiredSelections= [myStdPi0R,sels["sel_D0_KPi"]])
    sels["sel_Dstar0_Pi0M_KPi"]      = Selection(name = "sel_Dstar0_Pi0M_KPi",   Algorithm = CombineDstar02D0pi0, RequiredSelections= [myStdPi0M,sels["sel_D0_KPi"]])

    #=============================================#
    #=== Make D*0 -> D0(->Kpi) gamma candidates ==#
    #=============================================#
    CombineDstar02D0gamma = CombineParticles(DecayDescriptors = ["[D*(2007)0 -> D0 gamma]cc"],
    CombinationCut = "(ADAMASS('D*(2007)0') < 100.0)",
    MotherCut = "((M - M1) > -20) & ((M - M1) < 200) & (VFASPF(VCHI2PDOF) < 15)")
    sels["sel_Dstar0_gamma_KPi"]     = Selection(name = "sel_Dstar0_gamma_KPi",   Algorithm = CombineDstar02D0gamma, RequiredSelections= [myStdGamma,sels["sel_D0_KPi"]])

    #=============================================#
    #=== Various DDbar combinations, any fs ======#
    #=============================================#
    CombineD0D0bar      = CombineParticles(DecayDescriptors = ["psi(4040) -> D0 D~0"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDpDm         = CombineParticles(DecayDescriptors = ["psi(4040) -> D+ D-"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDspDsm       = CombineParticles(DecayDescriptors = ["psi(4040) -> D_s+ D_s-"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDstarpDstarm = CombineParticles(DecayDescriptors = ["psi(4040) -> D*(2010)+ D*(2010)-"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDstarpDm     = CombineParticles(DecayDescriptors = ["psi(4040) -> D*(2010)+ D-", "psi(4040) -> D*(2010)- D+"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDstar0D      = CombineParticles(DecayDescriptors = ["psi(4040) -> D*(2007)0 D~0", "psi(4040) -> D*(2007)~0 D0"],CombinationCut = "AALL",MotherCut = "ALL")
    CombineDstar0Dstar0 = CombineParticles(DecayDescriptors = ["psi(4040) -> D*(2007)0 D*(2007)~0"],CombinationCut = "AALL",MotherCut = "ALL")

    #=============================================#
    #=== Make selections & sequences =============#
    #=============================================#
    def makeSels(prefix, combinations, selDicts, combAlg):
        selsDict = {}
        # Loop through the combinations and make the `Selection' object
        for combination in combinations :
            input_1,input_2 = combination[0],combination[1]
            # Make required selections list
            requiredSelections=[]
            if(input_1==input_2): requiredSelections = [selDicts[input_1]];
            else: requiredSelections = [selDicts[input_1],selDicts[input_2]]
            # Return the selection
            sel = Selection(name="sel_"+prefix+input_1+"_"+input_2,   Algorithm = combAlg, RequiredSelections = requiredSelections)
            selsDict[prefix+input_1+"_"+input_2] = sel
        return selsDict

    # D0 D0bar combiner
    DDict = {   "KPi"  :      sels["sel_D0_KPi"]  ,
                "K3Pi" :      sels["sel_D0_K3Pi"] ,
                "KPi_WS":     sels["sel_D0_KPi_WS"] ,
                "Pi0R_KPi" :  sels["sel_Dstar0_Pi0R_KPi"] ,
                "Pi0M_KPi" :  sels["sel_Dstar0_Pi0M_KPi"] ,
                "gamma_KPi":  sels["sel_Dstar0_gamma_KPi"] }
    sels.update( makeSels("D0D0bar_", [ ["KPi","KPi"] , ["KPi","KPi_WS"] , ["KPi_WS","KPi"] , ["KPi_WS","KPi_WS"] ,
                                        ["KPi","K3Pi"] ,
                                        ["K3Pi","KPi"] ]
                                    , DDict, CombineD0D0bar) )
    # D+ D- combiner
    sels.update( makeSels("DpDm_"   , [ ["KPiPi","KPiPi"] ]
                                    , {"KPiPi":sels["sel_Dp_KPiPi"]}, CombineDpDm) )
    # Ds+ Ds- combiner
    sels.update( makeSels("DspDsm_" , [ ["KKPi","KKPi"] ]
                                    , {"KKPi":sels["sel_Dsp_KKPi"]}, CombineDspDsm) )
    # D*0 D0bar combiner
    sels.update( makeSels("Dstar0D0bar_", [ ["Pi0R_KPi","KPi"] , ["Pi0M_KPi","KPi"] , ["gamma_KPi","KPi"] ]
                                        , DDict , CombineDstar0D) )
    # D*0 D*0bar combiner
    sels.update( makeSels("Dstar0Dstar0bar_", [ ["Pi0R_KPi","Pi0R_KPi"]  , ["Pi0R_KPi","Pi0M_KPi"]  , ["Pi0R_KPi","gamma_KPi"] ,
                                                ["Pi0M_KPi","Pi0R_KPi"]  , ["Pi0M_KPi","Pi0M_KPi"]  , ["Pi0M_KPi","gamma_KPi"] ,
                                                ["gamma_KPi","Pi0R_KPi"] , ["gamma_KPi","Pi0M_KPi"] , ["gamma_KPi","gamma_KPi"] ]
                                            , DDict, CombineDstar0D) )

    # Build sequences
    seqs =[]
    for sel in sels.keys():
        seqName = sels[sel].name().replace("sel","seq");
        seq = SelectionSequence( seqName, TopSelection = sels[sel])
        seqs += [seq]

    #=============================================#
    #=== Pass sequences back =====================#
    #=============================================#
    return seqs
