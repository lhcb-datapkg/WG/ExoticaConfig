
def configure_ew_cep_dd_selection( year , isSimulation = False ) :

    ########################################
    from Configurables import DaVinci
    # Tuples
    from Configurables import EventTuple, DecayTreeTuple
    # Tuple tools
    from Configurables import TupleToolTISTOS, TupleToolStripping, TupleToolTrigger
    from Configurables import TupleToolDecay
    from Configurables import TupleToolVELOClusters, TupleToolVeloTrackClusterInfo

    # Import selection sequence builder
    from ew_cep_ddbar.makeCEP_DD_selectionSequences import makeSelectionSequences
    seqs = makeSelectionSequences()

    #=============================#
    #=== List of trigger lines ===#
    #=============================#

    triggerListAll = [ "L0HadronDecision", "L0DiHadron,lowMultDecision", "L0HRCDiHadron,lowMultDecision",
                       "Hlt1CEPVeloCutDecision",
                       "Hlt1NoPVPassThroughDecision","Hlt1LowMultPassThroughDecision","Hlt1LowMultDecision","Hlt1LowMultHerschelDecision","Hlt1LowMultVeloCut_HadronsDecision","Hlt1LowMultVeloAndHerschel_HadronsDecision",
                       "Hlt2LowMultD2KPiDecision", "Hlt2LowMultD2KPiPiDecision", "Hlt2LowMultD2K3PiDecision", "Hlt2LowMultChiC2HHDecision", "Hlt2LowMultChiC2HHHHDecision",
                       "Hlt2LowMultD2KPiWSDecision", "Hlt2LowMultD2KPiPiWSDecision", "Hlt2LowMultD2K3PiWSDecision", "Hlt2LowMultChiC2HHWSDecision", "Hlt2LowMultChiC2HHHHWSDecision",
                       "Hlt2LowMultDDIncDecision", "Hlt2LowMultDDIncCPDecision", "Hlt2LowMultDDIncVFDecision"
    ]

    #===============================#
    #=== List of stripping lines ===#
    #===============================#

    strippingList = [ "StrippingLowMultHadronLineDecision",
                    "StrippingLowMultHadronLinePSDecision",
                    "StrippingLowMultHadronLineNoFilterDecision",
                    "StrippingLowMultCEP_D2KPi_D2KPi_lineDecision",
                    "StrippingLowMultCEP_D2KPi_lineDecision",
                    "StrippingLowMultCEP_D2KPiPi_lineDecision",
                    "StrippingLowMultCEP_D2K3Pi_lineDecision",
                    "StrippingLowMultCEP_ChiC2HH_lineDecision",
                    "StrippingLowMultCEP_ChiC2PP_lineDecision",
                    "StrippingLowMultCEP_ChiC2HHHH_lineDecision",
                    "StrippingLowMultCEP_DD_lineDecision",
                    "StrippingLowMultCEP_KK_lineDecision",
                    "StrippingLowMultCEP_LMR2HH_lineDecision",
                    "StrippingLowMultCEP_D2KPiWS_lineDecision",
                    "StrippingLowMultCEP_D2KPiPiWS_lineDecision",
                    "StrippingLowMultCEP_D2K3PiWS_lineDecision"
    ]

    #=========================#
    #=== Set up EventTuple ===#
    #=========================#
    from Configurables import EventTuple
    etuple = EventTuple("EventTuple")
    etuple.ToolList = ["TupleToolEventInfo"]

    #==============================#
    #=== Set up DecayTreeTuples ===#
    #==============================#
    # First comes a dictionary that defines the various final states of all the charm particles
    # e.g. (D0, D+, D_s+, D*(2007)pi0R->gg, D*(2007)pi0M, D*(2007)gamma) etc
    # Necessary to explicitly conjugate the WS decay modes because D0 -> K+ pi+ is a *different
    # final state* to D0 -> K- pi-, but which we are folding together into a single "D0KPi_WS"
    decayDict = {}
    decayDict["D0KPi"]               ="( D0 -> ^K- ^pi+ )"
    decayDict["D0KPi_WS"]            ="( [ D0 -> ^K+ ^pi+ ]CC )"
    decayDict["D0K3Pi"]              ="( D0 -> ^K- ^pi+ ^pi- ^pi+ )"
    decayDict["D0KPiPi0R"]           ="( D0 -> ^K- ^pi+ ^pi0 )"
    decayDict["D0KPiPi0M"]           =decayDict["D0KPiPi0R"]
    decayDict["D0barKPi"]            ="( D~0 -> ^K+ ^pi- )"
    decayDict["D0barKPi_WS"]         ="( [ D~0 -> ^K- ^pi- ]CC )"
    decayDict["D0barK3Pi"]           ="( D~0 -> ^K+ ^pi- ^pi+ ^pi- )"
    decayDict["D0barKPiPi0R"]        ="( D~0 -> ^K+ ^pi- ^pi0 )"
    decayDict["D0barKPiPi0M"]        =decayDict["D0barKPiPi0R"]
    decayDict["DpKPiPi"]             ="( D+ -> ^K- ^pi+ ^pi+ )"
    decayDict["DmKPiPi"]             ="( D- -> ^K+ ^pi- ^pi- )"
    decayDict["DspKKPi"]             ="( D_s+ -> ^K+ ^K- ^pi+ )"
    decayDict["DsmKKPi"]             ="( D_s- -> ^K+ ^K- ^pi- )"
    decayDict["Dstar0gamma_KPi"]     ="( D*(2007)0 -> ^( D0 -> ^K- ^pi+ ) ^gamma )"
    decayDict["Dstar0bargamma_KPi"]  ="( D*(2007)~0 -> ^( D~0 -> ^K+ ^pi- ) ^gamma )"
    decayDict["Dstar0Pi0R_KPi"]      ="( D*(2007)0 -> ^( D0 -> ^K- ^pi+ ) ^( pi0 -> ^gamma ^gamma ) )"
    decayDict["Dstar0barPi0R_KPi"]   ="( D*(2007)~0 -> ^( D~0 -> ^K+ ^pi- ) ^( pi0 -> ^gamma ^gamma ) )"
    decayDict["Dstar0Pi0M_KPi"]      ="( D*(2007)0 -> ^( D0 -> ^K- ^pi+ ) ^pi0 )"
    decayDict["Dstar0barPi0M_KPi"]   ="( D*(2007)~0 -> ^( D~0 -> ^K+ ^pi- ) ^pi0 )"

    def makeDecayString_1D(prefix, fs):
        if prefix in ["D0_","Dp_"] :
            topLevel = decayDict[prefix[:-1]+fs][2:-2] # Strip outside ( and )
            topLevel = "( [ "+topLevel+" ]CC )"
        elif prefix == "Dstar_" :
            topLevel = "( [ D*(2010)+ -> ^"+decayDict["D0"+fs]+" ^pi+ ]CC )"
        elif prefix in ["Dstar0_Pi0R_","Dstar0_Pi0M_"] :
            topLevel = "( [ D*(2007)0 -> ^"+decayDict["D0"+fs]+" ^pi0 ]CC )"
        elif prefix == "Dstar0_gamma_" :
            topLevel = "( [ D*(2007)0 -> ^"+decayDict["D0"+fs]+" ^gamma ]CC )"
        else :
            raise ValueError('Failure in makeDecayString_1D. Could not find %s' % prefix)
        return topLevel

    # ================================================= #
    # This function makes the decay descriptors used to #
    # select what goes into the Tuple
    #
    # It expects to replace "psi -> X" first, according
    # to the value of the "prefix" parameter.
    # It then inserts the decay of the two D's, using
    # "input_1" and "input_2" as the guides
    # ================================================= #
    def makeDecayString(topLevel, prefix, input_1, input_2):
        # Sort head of decay chain
        if(prefix == "D0D0bar_"):          topLevel=topLevel.replace("X","^XD0 ^YD~0")
        if(prefix == "DstarDstar_"):       topLevel=topLevel.replace("X","^XD*(2010)+ ^YD*(2010)-")
        if(prefix == "DpDm_"):             topLevel=topLevel.replace("X","^XD+ ^YD-")
        if(prefix == "DspDsm_"):           topLevel=topLevel.replace("X","^XD_s+ ^YD_s-")
        if(prefix == "DstarD_"):           topLevel=topLevel.replace("X","^XD*(2010)+ ^YD-")
        if(prefix == "DDstar_"):           topLevel=topLevel.replace("X","^XD+ ^YD*(2010)-")
        if(prefix == "Dstar0D0bar_"):      topLevel=topLevel.replace("X","^XDstar0 ^YD~0")
        if(prefix == "D0Dstar0bar_"):      topLevel=topLevel.replace("X","^XD0 ^YDstar0bar")
        if(prefix == "Dstar0Dstar0bar_"):  topLevel=topLevel.replace("X","^XDstar0 ^YDstar0bar")
        # Sort secondaries
        if(topLevel.find("XD0")!=-1):   topLevel=topLevel.replace("XD0",decayDict["D0"+input_1])
        if(topLevel.find("XD~0")!=-1):  topLevel=topLevel.replace("XD~0",decayDict["D0bar"+input_1])
        if(topLevel.find("YD0")!=-1):   topLevel=topLevel.replace("YD0",decayDict["D0"+input_2])
        if(topLevel.find("YD~0")!=-1):  topLevel=topLevel.replace("YD~0",decayDict["D0bar"+input_2])
        if(topLevel.find("XD+")!=-1):   topLevel=topLevel.replace("XD+",decayDict["Dp"+input_1])
        if(topLevel.find("XD-")!=-1):   topLevel=topLevel.replace("XD-",decayDict["Dm"+input_1])
        if(topLevel.find("YD+")!=-1):   topLevel=topLevel.replace("YD+",decayDict["Dp"+input_2])
        if(topLevel.find("YD-")!=-1):   topLevel=topLevel.replace("YD-",decayDict["Dm"+input_2])
        if(topLevel.find("XD_s+")!=-1): topLevel=topLevel.replace("XD_s+",decayDict["Dsp"+input_1])
        if(topLevel.find("XD_s-")!=-1): topLevel=topLevel.replace("XD_s-",decayDict["Dsm"+input_1])
        if(topLevel.find("YD_s+")!=-1): topLevel=topLevel.replace("YD_s+",decayDict["Dsp"+input_2])
        if(topLevel.find("YD_s-")!=-1): topLevel=topLevel.replace("YD_s-",decayDict["Dsm"+input_2])
        if(topLevel.find("XDstar0")!=-1):     topLevel=topLevel.replace("XDstar0",decayDict["Dstar0"+input_1])
        if(topLevel.find("YDstar0bar")!=-1):  topLevel=topLevel.replace("YDstar0bar",decayDict["Dstar0bar"+input_2])
        return topLevel

    # ================================================= #
    # This function returns a dictionary of branches,
    # where the dictionary keys are the tuple branch names
    # and the values label the particle for that branch
    #
    # This works fine for the top-level particles.

    # Then it looks over the particles that might appear
    # more than once. It finds the last intermediate
    # particle, and attaches the kaon, pion or pi0 to
    # that "lastCompPart"
    # ================================================= #
    def getDecayBranches(prefix, dec ):
        _bDict = {}
        dec=dec.replace("^","");
        if(dec.find("psi")!=-1):        _bDict["psi"]     = "^"+dec
        if(dec.find("D0")!=-1):         _bDict["D0"]      = dec.replace("( D0","^( D0")
        if(dec.find("D~0")!=-1):        _bDict["D0bar"]   = dec.replace("( D~0","^( D~0")
        if(dec.find("D+")!=-1):         _bDict["Dp"]      = dec.replace("( D+","^( D+")
        if(dec.find("D-")!=-1):         _bDict["Dm"]      = dec.replace("( D-","^( D-")
        if(dec.find("D_s+")!=-1):       _bDict["Dsp"]     = dec.replace("( D_s+","^( D_s+")
        if(dec.find("D_s-")!=-1):       _bDict["Dsm"]     = dec.replace("( D_s-","^( D_s-")
        if(dec.find("D*(2010)+")!=-1):  _bDict["Dstp"]    = dec.replace("( D*(2010)+","^( D*(2010)+")
        if(dec.find("D*(2010)-")!=-1):  _bDict["Dstm"]    = dec.replace("( D*(2010)-","^( D*(2010)-")
        if(dec.find("D*(2007)0")!=-1):  _bDict["Dst0"]    = dec.replace("( D*(2007)0","^( D*(2007)0")
        if(dec.find("D*(2007)~0")!=-1): _bDict["Dst0bar"] = dec.replace("( D*(2007)~0","^( D*(2007)~0")
        # Now loop though pions and kaons
        lastCompPart=[]
        numK=0; numPi=0; numPiz=0; numgamma=0; # count pions or kaons found per composite particle
        posInList=-1;
        print dec.split()
        for frag in dec.split():
            posInList+=1 # Move position index of current fragment
            if(frag.find("D*(2010)+")!=-1): lastCompPart+=["Dstp"]
            if(frag.find("D*(2010)-")!=-1): lastCompPart+=["Dstm"]
            if(frag.find("D*(2007)0")!=-1): lastCompPart+=["Dst0"]
            if(frag.find("D*(2007)~0")!=-1):lastCompPart+=["Dst0bar"]
            if(frag.find("D0")!=-1):        lastCompPart+=["D0"]
            if(frag.find("D~0")!=-1):       lastCompPart+=["D0bar"]
            if(frag.find("D+")!=-1):        lastCompPart+=["Dp"]
            if(frag.find("D-")!=-1):        lastCompPart+=["Dm"]
            if(frag.find("D_s+")!=-1):      lastCompPart+=["Dsp"]
            if(frag.find("D_s-")!=-1):      lastCompPart+=["Dsm"]
            if(frag.find("pi0")!=-1):       lastCompPart+=["pi0"]
            if(frag == ')' and len(lastCompPart)>0): del lastCompPart[-1]; numK=0; numPi=0; numPiz=0; numgamma=0; # composite parent for pi and k
            if(frag.find('K')!=-1):         _bDict[lastCompPart[-1]+"_K"+str(numK+1)]   = ' '.join( dec.split()[:posInList] ) + ' ^'+frag+' ' + ' '.join( dec.split()[posInList+1:] );  numK+=1
            if(frag.find('pi+')!=-1 or frag.find('pi-')!=-1):        _bDict[lastCompPart[-1]+"_Pi"+str(numPi+1)]  = ' '.join( dec.split()[:posInList] ) + ' ^'+frag+' ' + ' '.join( dec.split()[posInList+1:] ); numPi+=1
            if(frag.find('pi0')!=-1):
                # Want to put the caret outside the brackets for (pi0->gamma gamma)
                print "I'm setting pi0 as: "
                print dec.split()
                if dec.split()[posInList-1]=="(" :
                    _bDict[lastCompPart[-2]+"_Piz"+str(numPiz+1)]  = ' '.join( dec.split()[:posInList-1] ) + ' ^( '+frag+' ' + ' '.join( dec.split()[posInList+1:] ); numPiz+=1
                else :
                    _bDict[lastCompPart[-2]+"_Piz"+str(numPiz+1)]  = ' '.join( dec.split()[:posInList] ) + ' ^'+frag+' ' + ' '.join( dec.split()[posInList+1:] ); numPiz+=1
            if(frag.find('gamma')!=-1):
                if lastCompPart[-1]=="pi0":
                    _bDict[lastCompPart[-2]+"_"+lastCompPart[-1]+"_gamma"+str(numgamma+1)]  = ' '.join( dec.split()[:posInList] ) + ' ^'+frag+' ' + ' '.join( dec.split()[posInList+1:] ); numgamma+=1
                else :
                    _bDict[lastCompPart[-1]+"_gamma"+str(numgamma+1)]  = ' '.join( dec.split()[:posInList] ) + ' ^'+frag+' ' + ' '.join( dec.split()[posInList+1:] ); numgamma+=1
        return _bDict

    def makeTuples_1D( prefix, fstates ):
        dictTuples={} # Dictionary to send back
        for fs in fstates:
            tup = DecayTreeTuple("tuple_"+prefix+fs);
            # Assign inputs
            for seq in seqs:
                if(seq.name() == "seq_"+prefix+fs):
                    tup.Inputs = [seq.outputLocation()];
                    # Set decay
                    tup.Decay = makeDecayString_1D(prefix,fs);
                    tup.addBranches( getDecayBranches( prefix, tup.Decay ) );
                    dictTuples[prefix+fs]=tup
        return dictTuples

    def makeTuples( prefix, combinations):
        listFScombs=[] # list of combs to check against
        dictTuples={}
        for combination in combinations :
            input_1 = combination[0]
            input_2 = combination[1]
            if input_2+input_1 in listFScombs:  continue # Ensure unique combination by checking reverse
            tup = DecayTreeTuple("tuple_"+prefix+input_1+"_"+input_2);
            # Assign Inputs
            for seq in seqs:
                if(seq.name() == "seq_"+prefix+input_1+"_"+input_2):
                    tup.Inputs = [seq.outputLocation()];
            # Set Decay
            parent='psi(4040)'
            decStr = ''
            if(input_1 != input_2): decStr=makeDecayString( "( [ "+parent+" -> X ]CC )", prefix, input_1, input_2 )
            else: decStr=makeDecayString( "( "+parent+" -> X )", prefix, input_1, input_2 ) #Don't CC the self-conjugate final state! Double-count!
            tup.Decay = decStr;
            tup.addBranches( getDecayBranches( prefix, tup.Decay ) );
            print "\n\nFor decay: "+tup.Decay
            for nickname,identifier in getDecayBranches( prefix, tup.Decay ).iteritems() :
                print nickname,identifier
            # Return
            dictTuples[prefix+input_1+"_"+input_2]=tup
            listFScombs+=[input_1+input_2];
        return dictTuples

    tupleDict={}
    # do 1D tuples
    tupleDict.update( makeTuples_1D("D0_",          ["KPi","KPi_WS","K3Pi","KPiPi0R","KPiPi0M"]) )
    tupleDict.update( makeTuples_1D("Dp_",          ["KPiPi"]) )
    tupleDict.update( makeTuples_1D("Dstar0_Pi0R_", ["KPi"]) )
    tupleDict.update( makeTuples_1D("Dstar0_Pi0M_", ["KPi"]) )
    tupleDict.update( makeTuples_1D("Dstar0_gamma_",["KPi"]) )
    # do 2D tuples
    tupleDict.update( makeTuples("D0D0bar_",         [ ["KPi","KPi"] , ["KPi","KPi_WS"] , ["KPi_WS","KPi"] , ["KPi_WS","KPi_WS"] ,
                                                       ["KPi","K3Pi"] ,
                                                       ["K3Pi","KPi"] ] ) )
    tupleDict.update( makeTuples("DpDm_",            [ ["KPiPi","KPiPi"] ] ) )
    tupleDict.update( makeTuples("DspDsm_",          [ ["KKPi","KKPi"] ]   ) )
    tupleDict.update( makeTuples("Dstar0D0bar_",     [ ["Pi0R_KPi","KPi"] , ["Pi0M_KPi","KPi"] , ["gamma_KPi","KPi"] ] ) )
    tupleDict.update( makeTuples("D0Dstar0bar_",     [ ["KPi","Pi0R_KPi"] , ["KPi","Pi0M_KPi"] , ["KPi","gamma_KPi"] ] ) )
    tupleDict.update( makeTuples("Dstar0Dstar0bar_", [ ["Pi0R_KPi","Pi0R_KPi"]  , ["Pi0R_KPi","Pi0R_KPi"]  , ["Pi0R_KPi","Pi0R_KPi"] ,
                                                       ["Pi0M_KPi","Pi0R_KPi"]  , ["Pi0M_KPi","Pi0R_KPi"]  , ["Pi0M_KPi","Pi0R_KPi"] ,
                                                       ["gamma_KPi","Pi0R_KPi"] , ["gamma_KPi","Pi0R_KPi"] , ["gamma_KPi","Pi0R_KPi"] ] ) )
    tupleListAll=[]
    for key in tupleDict.keys():
        tupleListAll+=[tupleDict[key]]

    #======================#
    #=== LoKi variables ===#
    #======================#
    def makeLoKiDTFVars(prefix, funcString, constraints, dictToExpand):
        dictToExpand["DOCAMAX"]="DOCAMAX";
        dictToExpand["AMAXDOCA"]="PFUNA(AMAXDOCA('LoKi::DistanceCalculator'))";
        if(prefix.find("psi")!=-1): #Add the following variables only once
            refitQuantitiesPrefix=prefix.replace("psi","");
            dictToExpand[refitQuantitiesPrefix+"CHI2"]="DTF_CHI2( False, "+constraints+" )";
            dictToExpand[refitQuantitiesPrefix+"NDOF"]="DTF_NDOF( False, "+constraints+" )";
            dictToExpand[refitQuantitiesPrefix+"CHI2NDOF"]="DTF_CHI2NDOF( False, "+constraints+" )";
        #Now add standard
        varList=['M','E','P','PX','PY','PZ','PT','ID']
        for var in varList:
            dictToExpand[prefix+"_"+var] = "DTF_FUN( "+funcString.replace('tbr',var)+", False, "+constraints+" )";

    lokipsiDTFvars={}
    # Setup refitting.
    # Refit D0D0bar always, and scan descriptor to add all the right daughters with right CHILD arguments
    # Refit D+D- always, as above
    # Refit the D+/D- in D+ D*-
    # Refit nothing in D* D*
    for tup in tupleListAll:
        print '\n\nFor tuple '+tup.name()
        # Only do this for D0D0bar, DpDm, DstarD, DstarDstar - no constraints for 1D
        nam = tup.name()[6:] # Skip 'tuple_' prefix
        if(nam[:nam.find("_")] not in ["D0D0bar","DpDm","DspDsm","Dstar0D0bar","D0Dstar0bar","Dstar0Dstar0bar"]):
            continue
        # For all the pairs, continue now to add constraints
        dec = tup.Decay.replace('^','')
        dec = dec.split()
        print dec
        decay = [x for x in dec if not (x=='[' or x==']' or x==']CC')];
        posInList=0
        lastCompPart=[]
        # Add appropriate constraints (combo of with and without pi0 and Dst constraints where appropriate. NB: 'DstarD' catches both D*D and D*D*!
        lokStrConstraints=''
        if(tup.name().find('D0D0bar')!=-1):
            lokStrConstraints = ["strings('D0','D~0')", "strings('D0','D~0','pi0')", "strings('D0','D~0','pi0','KS0')"]
        if(tup.name().find('DpDm')!=-1):
            lokStrConstraints = ["strings('D+','D-')",  "strings('D+','D-','pi0')", "strings('D+','D-','pi0','KS0')"]
        if(tup.name().find('Dstar')!=-1):
            lokStrConstraints = ["strings('D+','D-','D0','D~0')",  "strings('D+','D-','D0','D~0','pi0')", "strings('D+','D-','D0','D~0','pi0','KS0')",
                                 "strings('D*(2007)0','D*(2007)~0','D*(2010)+','D*(2010)-','D+','D-','D0','D~0')",
                                 "strings('D*(2007)0','D*(2007)~0','D*(2010)+','D*(2010)-','D+','D-','D0','D~0','pi0')", "strings('D*(2010)+','D*(2010)-','D+','D-','D0','D~0','pi0','KS0')"
                                ]

        # Now loop through and make the dtf
        levelInChain=0
        nDauAtLevel = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0} #Assume decay chains only have at most 5 layers
        pDic = {'psi(4040)':'psi', 'psi(3770)':'psi', 'D+':'Dp', 'D-':'Dm', 'D_s+':'Dsp', 'D_s-':'Dsm', 'D0':'D0', 'D~0':'D0bar', 'D*(2010)+':'Dstp', 'D*(2010)-':'Dstm', 'D*(2007)0':'Dstar0','D*(2007)~0':'Dstar0bar','pi0':'Piz', 'gamma':'gamma', 'pi+':'Pi', 'pi-':'Pi', 'K+':'K', 'K-':'K','KS0':'KS0'}
        lokipsiDTFvars[tup.name()] = {}
        numK=0; numPi=0; numPiz=0;
        for frag in decay:
            # Get composite particles for naming kaons and pions
            if(frag.find("D*(2010)+")!=-1): lastCompPart+=["Dstp"];
            if(frag.find("D*(2010)-")!=-1): lastCompPart+=["Dstm"];
            if(frag.find("D*(2007)0")!=-1): lastCompPart+=["Dstar0"];
            if(frag.find("D*(2007)~0")!=-1): lastCompPart+=["Dstar0bar"];
            if(frag.find("D0")!=-1):        lastCompPart+=["D0"];
            if(frag.find("D~0")!=-1):       lastCompPart+=["D0bar"];
            if(frag.find("D+")!=-1):        lastCompPart+=["Dp"];
            if(frag.find("D-")!=-1):        lastCompPart+=["Dm"];
            if(frag.find("D_s+")!=-1):      lastCompPart+=["Dsp"];
            if(frag.find("D_s-")!=-1):      lastCompPart+=["Dsm"];
            if(frag == ')' and len(lastCompPart)>0): del lastCompPart[-1]; numK=0; numPi=0; numPiz=0; # composite parent for pi and k
            # Determine location in chain and prepare LoKi DTF string
            if(frag=='->'):  levelInChain+=1;
            if(frag==')'):
                nDauAtLevel[levelInChain]=0;
                levelInChain-=1;
            if(frag not in ['(','->',')']):
                nDauAtLevel[levelInChain]+=1; # keep record of position in the decay at each level
                lokStr_sub = '';  lokStr_lab=''
                if(levelInChain==0):
                    lokStr_lab="DTF_psi"
                    lokStr_sub='tbr'
                else:
                    if(pDic[frag]=='Pi'):
                        numPi+=1
                        lokStr_lab = "DTF_"+lastCompPart[-1]+pDic[frag]+str(numPi)
                    elif(pDic[frag]=='K'):
                        numK+=1;
                        lokStr_lab = "DTF_"+lastCompPart[-1]+pDic[frag]+str(numK)
                    elif(pDic[frag]=='Piz'):
                        numPiz+=1;
                        lokStr_lab = "DTF_"+lastCompPart[-1]+pDic[frag]+str(numPiz)
                    else: lokStr_lab = "DTF_"+pDic[frag]
                    # Now prepare loki string
                    sufStr='' # To close brackets
                    for i in range(1,levelInChain+1,1):
                        lokStr_sub += 'CHILD('+str(nDauAtLevel[i])+', ';
                        sufStr += ')'
                    lokStr_sub = lokStr_sub + 'tbr'+sufStr
                # Put it altogether! [in the loop:
                    # 0 is D0 constraint and no pi0 constraint and no Dst constraint
                    # 1 is D0 constraint and pi0 constraint and no Dst constraint
                    # 2 is D0 constraint and Dst constraint and no pi0 constraint
                    # 3 is D0 constraint and Dst constraint and pi0 constraint
                for i in range(0,len(lokStrConstraints),1):
                    if(i==1): lokStr_lab=lokStr_lab.replace('DTF','DTFpiz')
                    if(i==2): lokStr_lab=lokStr_lab.replace('DTF','DTFDst')
                    if(i==3): lokStr_lab=lokStr_lab.replace('DTF','DTFDstpiz')
                    makeLoKiDTFVars(lokStr_lab , lokStr_sub, lokStrConstraints[i], lokipsiDTFvars[tup.name()])

    #======================#
    #=== Load nTuples =====#
    #======================#
    for ntp in tupleListAll:
        ntp.ToolList += [ "TupleToolAngles",
                        "TupleToolEventInfo",
                        "TupleToolGeometry",
                        "TupleToolKinematic",
                        "TupleToolL0Data",
#                      "TupleToolPi0Info",
                        "TupleToolPid",
                        "TupleToolPrimaries",
                        "TupleToolParticleStats",
                        "TupleToolPhotonInfo",
                        "TupleToolRecoStats",
                        "TupleToolTrackInfo",
                        "TupleToolAllVeloTracks",
                        "TupleToolAllPhotons"
                        ]
        if year == "2015" :
          tth_uncorrected  = ntp.addTupleTool("TupleToolHerschel/tth_uncorrected")
          tth_uncorrected.ExtraName = "Uncorrected"
          tth_corrected    = ntp.addTupleTool("TupleToolHerschel/tth_corrected")
          tth_corrected.DigitsLocation = "Raw/HC/CorrectedDigits"
        elif year in ["2016","2017","2018"] :
          tth = ntp.addTupleTool("TupleToolHerschel/tth")

        #ttvc = ntp.addTupleTool("TupleToolVELOClusters/myttvc")
        #ttvc.VeloClusterLocation = "Velo/RawEvent/Clusters"
        #ttvtci = ntp.addTupleTool("TupleToolVeloTrackClusterInfo/myttvtci")

        # Set the trigger decisions to appear for the ground-state D particles
        for trigPart in ["D0","D0bar","Dp","Dm","Dsp","Dsm"] :
            ntp.addTool(TupleToolDecay, name=trigPart)
            tttt = getattr(ntp,trigPart).addTupleTool("TupleToolTISTOS")
            tttt.VerboseL0 = True
            tttt.VerboseHlt1 = True
            tttt.VerboseHlt2 = True
            tttt.TriggerList = triggerListAll

        #TupleToolStripping
        tts = ntp.addTupleTool("TupleToolStripping")
        tts.StrippingList = strippingList

        #LoKi DTF
        print lokipsiDTFvars
        if(ntp.Decay.find("psi")!=-1):
            LoKi_psi = ntp.psi.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_psi")
            LoKi_psi.Variables = lokipsiDTFvars[ntp.name()];
        # Other LoKi variables
        if(ntp.Decay.find("D0")!=-1):
            LoKi_D0   = ntp.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
            LoKi_D0.Variables = {"AMAXDOCA" : "PFUNA(AMAXDOCA('LoKi::DistanceCalculator'))"}
        if(ntp.Decay.find("D~0")!=-1):
            LoKi_D0bar= ntp.D0bar.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0bar")
            LoKi_D0bar.Variables = {"AMAXDOCA" : "PFUNA(AMAXDOCA('LoKi::DistanceCalculator'))"}

    #=====================================#
    #=== Do scaling of tracks ============#
    #=====================================#
    from Configurables import TrackScaleState as SCALER
    scaler= SCALER('Scaler')
    from Configurables import TrackSmearState as SMEAR
    smear = SMEAR( 'Smear' )
    
    DaVseqList=[]
    if ( not isSimulation ) :
        DaVseqList = [scaler]
    else :
        DaVseqList = [smear]
    for seq in seqs :
        DaVseqList.append( seq.sequence()  )

    if year == "2015" :
      from Configurables import HCRawBankDecoder
      from Configurables import HCDigitCorrector
      DaVseqList += [HCRawBankDecoder(),HCDigitCorrector()]
    elif year in ["2016","2017","2018"] :
      from Configurables import HCRawBankDecoder
      DaVseqList += [HCRawBankDecoder()]
    DaVseqList += tupleListAll

    algorithmDict = {}
    algorithmDict["selTuples"] = DaVseqList # Algorithms that can be run after a filter
    algorithmDict["eventTuple"] = [etuple]  # Algorithms that should be run outside of the filter

    return algorithmDict
