'''
This module prepares and returns event filters appropriately
'''
 
def getFilters( the_year , the_sample ) :
  from PhysConf.Filters import LoKi_Filters

  # Define filters
  NoBiasStripFilter = LoKi_Filters( STRIP_Code="HLT_PASS('StrippingMBNoBiasDecision')")

  NonBeamBeamStripFilter = LoKi_Filters( STRIP_Code="HLT_PASS('StrippingH.*1NoBiasNonBeamBeamLineDecision') | HLT_PASS('StrippingLowMultNonBeamBeamNoBiasLineDecision')" )

  MuonStripFilter = LoKi_Filters( STRIP_Code="HLT_PASS('StrippingLowMultMuonLineDecision') | \
                                HLT_PASS('StrippingLowMultDiMuonLineDecision')" )

  LongTrackFilter = LoKi_Filters (
          VOID_Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 6 )" ,
          VOID_Preambulo =  ["from LoKiTracks.decorators import *"]
  )

  # Return appropriately
  list_to_return = []

  if the_year in ["2015","2016"] :
    if the_sample=="ContinuumDimuon" :
      list_to_return = MuonStripFilter.filters('MuonStripFilter')+LongTrackFilter.filters('LongTrackFilter') 
    elif the_sample=="NonBeamBeam" :
      list_to_return = NonBeamBeamStripFilter.filters('NonBeamBeamStripFilter')+LongTrackFilter.filters('LongTrackFilter') 
    else : # implicitly "NoBias"
      list_to_return = NoBiasStripFilter.filters('NoBiasStripFilter')
  elif the_year in ["2017","2018"] :
    if the_sample=="ContinuumDimuon" :
      list_to_return = LongTrackFilter.filters('LongTrackFilter') 
    elif the_sample=="NonBeamBeam" :
      list_to_return = LongTrackFilter.filters('LongTrackFilter') 
    else : # implicitly "NoBias"
      list_to_return = []

  return list_to_return
