
def configure_hrc_pedestals_and_dimu_selection( dataYear , sample ) :

  from PhysSelPython.Wrappers import SelectionSequence, Selection
  from Configurables import CombineParticles
  # Trigger
  #from Configurables import L0TriggerTisTos, TriggerTisTos
# Tuple tools
  #from Configurables import TupleToolPropertime, TupleToolTISTOS, TupleToolStripping, TupleToolTrigger
  #from Configurables import LoKi__Hybrid__TupleTool, TupleToolDecay, TupleToolTrackIsolation
# Selection tools
  #from DecayTreeTuple.Configuration import *
  #from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand
  #from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles
  
  #=======================#
  #=== Make candidates ===#
  #=======================#
  from StandardParticles import StdAllNoPIDsMuons as _stdNoPIDsMuons
   
  _JpsimumuDauCuts = { "mu-": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)", "mu+": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)" }
  _JpsimumuCombCut = "(AM>0)"
  _Jpsimumu = CombineParticles( name = "MyJpsi2MuMu" ,
                                DecayDescriptors = [ "J/psi(1S) -> mu- mu+"],
                                DaughtersCuts = _JpsimumuDauCuts,
                                CombinationCut = _JpsimumuCombCut ,
                                MotherCut = 'P>0.'   )
  Jpsimumu = Selection ( name="SelJpsMuMu",  Algorithm = _Jpsimumu,  RequiredSelections = [_stdNoPIDsMuons]    )
  seqDiMu = SelectionSequence( "seq_LMR2DiMu", TopSelection = Jpsimumu);

  #=============================#
  #=== List of trigger lines ===#
  #=============================#
  mystrips   = [  "StrippingMBNoBiasDecision", "StrippingHlt1NoBiasNonBeamBeamLineDecision", "StrippingLowMultNonBeamBeamNoBiasLineDecision", 
                  "StrippingLowMultMuonLineDecision", "StrippingLowMultDiMuonLineDecision" ]

  triggerListAll = [ "L0Muon,lowMultDecision","L0DiMuon,lowMultDecision","L0DiHadron,lowMultDecision",
                     "Hlt1NoPVPassThroughDecision", "Hlt1CEPVeloCutDecision", 
                     "Hlt1NoBiasNonBeamBeamDecision", "Hlt2NoBiasNonBeamBeamDecision","Hlt2LowMultTechnical_MinBiasDecision",
                     "Hlt2LowMultMuonDecision","Hlt2LowMultDiMuonDecision"]
  
  #=========================#
  #=== Set up EventTuple ===#
  #=========================#
  from Configurables import EventTuple
  etuple = EventTuple("EventTuple")
  etuple.ToolList = ["TupleToolEventInfo",
                     "TupleToolL0Data",
                     "TupleToolRecoStats",
                     "TupleToolFillingScheme"]
  et_ttt=etuple.addTupleTool("TupleToolTrigger");
  et_ttt.VerboseL0=True
  et_ttt.VerboseHlt1=True
  et_ttt.VerboseHlt2=True
  et_ttt.TriggerList=triggerListAll
  et_tth=etuple.addTupleTool("TupleToolHerschel")
  if dataYear == "2015" :
    et_tth.DigitsLocation = "Raw/HC/CorrectedDigits"
  else :
    et_tth.DigitsLocation = "Raw/HC/Digits"
  et_tth.CrateB = 0
  et_tth.CrateF = 1
  et_tts = etuple.addTupleTool("TupleToolStripping")
  et_tts.StrippingList = mystrips

  #==============================#
  #=== Set up DecayTreeTuples ===#
  #==============================#
  from Configurables import DecayTreeTuple
  from Configurables import TupleToolDecay
  ntp_MuMu = DecayTreeTuple("tuple_Jpsi2MuMu");
  ntp_MuMu.Inputs = [Jpsimumu.outputLocation()]
  ntp_MuMu.Decay = "( J/psi(1S) -> ^mu+ ^mu-)"
  ntp_MuMu.Branches = {
    "muplus"  :    "J/psi(1S) -> ^mu+  mu-",
    "muminus" :    "J/psi(1S) ->  mu+ ^mu-",
    "DiMu"    :    " ^(J/psi(1S) -> mu+ mu-)"
  }
  ntpList=[ntp_MuMu]

  #======================#
  #=== Load nTuples =====#
  #======================#
  for ntp in ntpList:
    ntp.ToolList += [ "TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolL0Data",
#                      "TupleToolPi0Info",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolParticleStats",
                      "TupleToolPhotonInfo",
                      "TupleToolRecoStats",
                      "TupleToolTrackInfo",
#                    "TupleToolVELOClusters",
#                    "TupleToolVeloTrackClusterInfo",
                      "TupleToolAllVeloTracks",
#                    "TupleToolAllPhotons"
                      "TupleToolFillingScheme"
                      ]

    ttt=ntp.addTupleTool("TupleToolTrigger");
    ttt.VerboseL0=True
    ttt.VerboseHlt1=True
    ttt.VerboseHlt2=True
    ttt.TriggerList=triggerListAll
    tth=ntp.addTupleTool("TupleToolHerschel")
    if dataYear == "2015" :
      tth.DigitsLocation = "Raw/HC/CorrectedDigits"
    else :
      tth.DigitsLocation = "Raw/HC/Digits"
    tth.CrateB = 0
    tth.CrateF = 1
    
    ntp.addTool(TupleToolDecay, name="DiMu")
    
    #TupleToolTISTOS
    tttt = ntp.DiMu.addTupleTool("TupleToolTISTOS")
    tttt.VerboseL0 = True
    tttt.VerboseHlt1 = True
    tttt.VerboseHlt2 = True
    tttt.TriggerList = triggerListAll

    #TupleToolStripping
    tts = ntp.addTupleTool("TupleToolStripping")
    tts.StrippingList = mystrips

  # Decode the Herschel raw bank 
  from Configurables import HCRawBankDecoder
  decoder = HCRawBankDecoder() 
  seqToReturn = [ decoder ]

  # If year is 2015, employ the calibration to remove common noise
  if dataYear == "2015" :
    from Configurables import HCDigitCorrector
    digitCorrector = HCDigitCorrector()
    seqToReturn += [ digitCorrector ]

  # Add the EventTuple for all samples
  seqToReturn += [ etuple ]

  # Only add the dimuon builder for ContinuumDimuon sample types 
  if sample == "ContinuumDimuon" :
    seqToReturn += [ seqDiMu.sequence() , ntp_MuMu ]   

  return seqToReturn

